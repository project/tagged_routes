<?php

namespace Drupal\tagged_routes;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a route tag entity type.
 */
interface RouteTagInterface extends ConfigEntityInterface {

}
