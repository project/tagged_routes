<?php

namespace Drupal\tagged_routes\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\tagged_routes\RouteTagInterface;

/**
 * Defines the route tag entity type.
 *
 * @ConfigEntityType(
 *   id = "route_tag",
 *   label = @Translation("Route tag"),
 *   label_collection = @Translation("Route tags"),
 *   label_singular = @Translation("route tag"),
 *   label_plural = @Translation("route tags"),
 *   label_count = @PluralTranslation(
 *     singular = "@count route tag",
 *     plural = "@count route tags",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\tagged_routes\RouteTagListBuilder",
 *     "form" = {
 *       "add" = "Drupal\tagged_routes\Form\RouteTagForm",
 *       "edit" = "Drupal\tagged_routes\Form\RouteTagForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "route_tag",
 *   admin_permission = "administer route_tag",
 *   links = {
 *     "collection" = "/admin/structure/route-tag",
 *     "add-form" = "/admin/structure/route-tag/add",
 *     "edit-form" = "/admin/structure/route-tag/{route_tag}",
 *     "delete-form" = "/admin/structure/route-tag/{route_tag}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "path",
 *     "tag"
 *   }
 * )
 */
class RouteTag extends ConfigEntityBase implements RouteTagInterface {

  /**
   * The route tag ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The route tag label.
   *
   * @var string
   */
  protected $label;

  /**
   * The route tag path.
   *
   * @var bool
   */
  protected $path;

  /**
   * The route_tag tag.
   *
   * @var string
   */
  protected $tag;

}
