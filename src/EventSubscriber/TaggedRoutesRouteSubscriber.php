<?php

namespace Drupal\tagged_routes\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Tagged Routes route subscriber.
 */
class TaggedRoutesRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($collection->all() as $route) {
      $route_tags = \Drupal::entityTypeManager()
        ->getStorage('route_tag')
        ->loadByProperties(['path' => $route->getPath()]);

      $new_tags = [];
      foreach ($route_tags as $route_tag) {
        $new_tags[] = $route_tag->get('tag');
      }

      if (count($new_tags)) {
        $tags = [];

        // Get previously defined route tags.
        if ($route->hasOption('tags')) {
          $tags = $route->getOption('tags');
        }

        // Add additional tags.
        $tags = array_merge($tags, $new_tags);
        $tags = array_unique($tags);
        $route->setOption('tags', $tags);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -300];
    return $events;
  }

}
