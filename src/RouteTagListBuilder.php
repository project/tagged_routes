<?php

namespace Drupal\tagged_routes;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of route tags.
 */
class RouteTagListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['tag'] = $this->t('Tag');
    $header['path'] = $this->t('Path');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\tagged_routes\RouteTagInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['tag'] = $entity->get('tag');
    $row['path'] = $entity->get('path');
    return $row + parent::buildRow($entity);
  }

}
